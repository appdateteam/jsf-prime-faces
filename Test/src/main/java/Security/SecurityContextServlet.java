package Security;

import java.io.IOException;

import javax.inject.Inject;
import javax.security.enterprise.SecurityContext;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/securityContextServlet")
@ServletSecurity(@HttpConstraint(rolesAllowed = "admin"))
public class SecurityContextServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Inject
	private SecurityContext securityContext;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		String contextName = null;
	       if (securityContext.getCallerPrincipal() != null) {
	           contextName = securityContext.getCallerPrincipal().getName();
	       }
	       response.getWriter().write("context username: " + contextName + "\n");

	}
}
