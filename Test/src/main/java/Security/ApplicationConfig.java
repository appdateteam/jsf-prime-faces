package Security;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.annotation.FacesConfig;
import javax.security.enterprise.authentication.mechanism.http.AutoApplySession;
import javax.security.enterprise.authentication.mechanism.http.CustomFormAuthenticationMechanismDefinition;
import javax.security.enterprise.authentication.mechanism.http.LoginToContinue;

import javax.security.enterprise.identitystore.DatabaseIdentityStoreDefinition;
import javax.security.enterprise.identitystore.Pbkdf2PasswordHash;

@AutoApplySession
@CustomFormAuthenticationMechanismDefinition(
		loginToContinue = @LoginToContinue(
				loginPage = "/login.xhtml", 
				errorPage = "error.xhtml",useForwardToLogin = false))
@DatabaseIdentityStoreDefinition(
		dataSourceLookup = "jdbc/StudentsTestPool", 
		callerQuery = "select password from users where USERNAME = ?", 
		groupsQuery = "select groups.GROUPNAME\r\n" + 
				"from groups\r\n" + 
				"inner join users on groups.GROUP_ID = users.GROUPS where users.USERNAME = ?",
		hashAlgorithm = Pbkdf2PasswordHash.class,
		hashAlgorithmParameters = {
				"Pbkdf2PasswordHash.Iterations=3072",
				"Pbkdf2PasswordHash.Algorithm=PBKDF2WithHmacSHA512",
				"Pbkdf2PasswordHash.SaltSizeBytes=64"})

@FacesConfig
@ApplicationScoped
public class ApplicationConfig {
	
}
