package EJB;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import Entities.Customer;

@Stateful
@LocalBean
public class CustomerDaoBean {

	@PersistenceContext(unitName = "StudentsTest")
	private EntityManager entityManager;

	public List<Customer> findAll() {
		CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
		cq.select(cq.from(Customer.class));
		return entityManager.createQuery(cq).getResultList();
	}

	public void saveCustomer(Customer customer) {
		if (customer.getId() == null) {
			saveNewCustomer(customer);
		} else {
			updateCustomer(customer);
		}
	}

	private void saveNewCustomer(Customer customer) {
		entityManager.persist(customer);
	}

	public void updateCustomer(Customer customer) {

		entityManager.merge(customer);

	}

	public Customer getCustomer(Long customerId) {
		Customer customer;

		customer = entityManager.find(Customer.class, customerId);

		return customer;
	}

	public void deleteCustomer(Customer cu) {
		System.out.println(cu.getFirstName());
		Customer cust = entityManager.merge(cu);
		entityManager.remove(cust);
	}
}