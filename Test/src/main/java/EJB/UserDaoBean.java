package EJB;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.security.enterprise.SecurityContext;

import com.sun.istack.NotNull;
import com.sun.xml.ws.security.impl.policy.UsernameToken;

import Entities.Groups;
import Entities.User;
import Entities.User_;

@Stateful
@LocalBean
public class UserDaoBean {

	@PersistenceContext(unitName = "StudentsTest")
	private EntityManager entityManager;

	public List<User> findAll() {
		CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
		cq.select(cq.from(Groups.class));
		cq.select(cq.from(User.class));
		System.out.println(entityManager.createQuery(cq).getResultList());
		return entityManager.createQuery(cq).getResultList();
	}

	public User findUserByUsername(@NotNull final String username) {

		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<User> criteria = builder.createQuery(User.class);
		Root<User> from = criteria.from(User.class);
		criteria.select(from);
		criteria.where(builder.equal(from.get(User_.username), username));
		TypedQuery<User> typed = entityManager.createQuery(criteria);
		try {
			return typed.getSingleResult();
		} catch (final NoResultException nre) {
			return null;
		}
	}
	
	public User findUsersPassword(@NotNull final String password) {

		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<User> criteria = builder.createQuery(User.class);
		Root<User> from = criteria.from(User.class);
		criteria.select(from);
		criteria.where(builder.equal(from.get(User_.password), password));
		TypedQuery<User> typed = entityManager.createQuery(criteria);
		try {
			return typed.getSingleResult();
		} catch (final NoResultException nre) {
			return null;
		}
	}

	public void saveUser(User user) {
		if (user.getUserId() == null) {
			saveNewUser(user);
		} else {
			updateUser(user);
		}
	}

	private void saveNewUser(User user) {
		entityManager.persist(user);
	}

	public void updateUser(User user) {

		if (!entityManager.contains(user)) {
			user = entityManager.merge(user);
		}
		entityManager.merge(user);

	}

	public User getUser(Long userId) {
		User user;

		user = entityManager.find(User.class, userId);

		return user;
	}

	public void deleteUser(User us) {
		
		if (!entityManager.contains(us)) {
			us = entityManager.merge(us);
		}
		
		entityManager.remove(us);
	}

}
