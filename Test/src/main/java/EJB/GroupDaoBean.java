package EJB;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Entities.Groups;
import Entities.User;

@Stateful
@LocalBean
public class GroupDaoBean {

	@PersistenceContext(unitName = "StudentsTest")
	private EntityManager entityManager;

	public List<Groups> findAll() {
		CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
		cq.select(cq.from(Groups.class));
		return entityManager.createQuery(cq).getResultList();
	}

	public Groups getGroup(Integer groupId) {
		Groups groups;

		groups = entityManager.find(Groups.class, groupId);

		return groups;
	}
	
	public void saveGroup(Groups groups) {
		if (groups.getGroupId() == null) {
			saveNewGroup(groups);
		} else {
			updateGroup(groups);
		}
	}
	
	private void saveNewGroup(Groups groups) {
		entityManager.persist(groups);
	}

	public void updateGroup(Groups groups) {

		entityManager.merge(groups);

	}
	
	
	/*public Groups findUserByUsername(String username) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery cq = cb.createQuery();
		Root<Groups> groups = cq.from(Groups.class);
		cq.where((cb.equal(groups.get(Groups_.userName), username)));
		cq.select(groups);
		TypedQuery<Groups> q = entityManager.createQuery(cq);
		Groups result  = q.getSingleResult();
		
		return result;
	}*/

}