package Controllers;



import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.security.enterprise.CallerPrincipal;

import Entities.User;

@Named
@SessionScoped
public class CustomPrincipal extends CallerPrincipal {

	private final User user;

	public CustomPrincipal(User user) {
		super(user.getUsername());
		this.user = user;
	}

	public User getUser() {
		return user;
	}
}