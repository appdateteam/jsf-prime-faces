package Controllers;

import java.io.IOException;
import java.io.Serializable;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import javax.inject.Inject;
import javax.inject.Named;
import javax.security.enterprise.AuthenticationStatus;
import javax.security.enterprise.SecurityContext;
import javax.security.enterprise.authentication.mechanism.http.AuthenticationParameters;

import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import EJB.GroupDaoBean;
import EJB.UserDaoBean;


@Named(value = "loginController")
@SessionScoped
public class LoginController implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EJB
	GroupDaoBean groupDaoBean;
	@EJB
	UserDaoBean userDaoBean;

	@Inject
	private SecurityContext securityContext;
	@Inject
	private ExternalContext externalContext;
	@Inject
	private FacesContext facesContext;
	@NotEmpty
	@Size(min = 4, message = "Password must be at least 4 characters")
	private String password;

	@NotEmpty
	@Size(min = 4, message = "Username must be at least 4 characters")
	private String username;
	
	private String message = "";
	
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	
	public void login() throws IllegalStateException, IOException {

		switch (continueAuthentication()) {
		case SEND_CONTINUE:
			facesContext.responseComplete();
			break;
		case SEND_FAILURE:
			warn();
			addError(facesContext, "Authentication failed");
			break;
		case SUCCESS:
			info();
			try {		
				System.out.println("HELLO!!!!"+securityContext.getCallerPrincipal().getName());
				externalContext.redirect(externalContext.getRequestContextPath() + "/app/success_login.xhtml?faces-redirect=true");
			} catch (IOException e) {

				e.printStackTrace();
			}
			
			break;
		case NOT_DONE:
			fatal();
		}
	}
	
	private void addError(FacesContext facesContext2, String string) {

	}

	public void info() {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Login succeed"));
	}

	public void warn() {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_WARN, "Login failed", null));
	}

	public void error() {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error Username or Password", "Login failed"));
	}

	public void fatal() {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_FATAL, "Fatal!", "System Error"));
	}
	
	public void logoutMes() {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Info: ", "Logged out Successfully"));
	}

	public void logout() {
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().invalidateSession();
		try {
			context.getExternalContext().redirect(externalContext.getRequestContextPath() + "/index.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private AuthenticationStatus continueAuthentication() {
		
		return securityContext.authenticate((HttpServletRequest) externalContext.getRequest(),
				(HttpServletResponse) externalContext.getResponse(),
				AuthenticationParameters.withParams().credential(new UsernamePasswordCredential(username, password)));

	}
	
}