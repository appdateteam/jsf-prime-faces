package Controllers;

import java.io.Serializable;


import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import java.util.List;
import javax.ejb.EJB;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.security.enterprise.SecurityContext;

import EJB.CustomerDaoBean;
import Entities.Customer;


@Named
@SessionScoped
public class CustomerController implements Serializable {


	private static final long serialVersionUID = 1L;

	@EJB
	private CustomerDaoBean customerDaoBean;

	private Customer customer;
	private Customer selected;
	private List<Customer> customers = null;
	private String firstName;
	private String lastName;
	private String email;
	
	@Inject
	private SecurityContext securityContext;
	
	@Inject
	private LoginController loginController; 
	

	public CustomerController() {
		customer = new Customer();
	}

	public String saveCustomer() {
		String returnValue = "customer_saved";

		try {
			populateCustomer();
			customerDaoBean.saveCustomer(customer);
		} catch (Exception e) {
			e.printStackTrace();
			returnValue = "error_saving_customer";
		}

		return returnValue;
	}

	public void deleteCustomerMessage() {

		addMessage("System Error", "Please try again later.");
	}

	public void addMessage(String summary, String detail) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	private void populateCustomer() {
		if (customer == null) {
			customer = new Entities.Customer();
		}
		customer.setFirstName(getFirstName());
		customer.setLastName(getLastName());
		customer.setEmail(getEmail());
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	

	public LoginController getLoginController() {
		return loginController;
	}

	public void setLoginController(LoginController loginController) {
		this.loginController = loginController;
	}

	public Customer getSelected() {
		return selected;
	}

	public void setSelected(Customer selected) {
		this.selected = selected;
		System.out.println(this.selected.getFirstName());
	}

	public List<Customer> getCustomers() {
		customers = customerDaoBean.findAll();
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

	public String deleteCustomer(Customer cu) {
		String returnValue = "customer_list";
		try {
			customerDaoBean.deleteCustomer(cu);
		} catch (Exception e) {
			e.printStackTrace();
			returnValue = "error_saving_customer";
		}

		return returnValue;
	}
	public String updateCustomer(Customer cu) {
		String returnValue = "update_customer";
		setSelected(cu);
		this.setSelected(getSelected());
		
		return returnValue;
	}
	
	public String saveUpdated() {
		String returnValue = "customer_updated";
		try {
			customerDaoBean.updateCustomer(this.selected);
		} catch (Exception e) {
			e.printStackTrace();
			returnValue = "error_saving_customer";
		}
		
		return returnValue;
	}


	public SecurityContext getSecurityContext() {
		return securityContext;
	}

	public void setSecurityContext(SecurityContext securityContext) {
		this.securityContext = securityContext;
	}

	@Override
	public String toString() {
		return "CustomerController [securityContext=" + securityContext + "]";
	}
	
	public String moveTo() {
		return "/app/customer_list";
	}
	
	
}