package Controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.security.enterprise.SecurityContext;

import EJB.GroupDaoBean;
import EJB.UserDaoBean;
import Entities.Groups;
import Entities.User;
import util.PasswordHasher;

@Named
@SessionScoped
public class UserController implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private UserDaoBean userDaoBean;
	@EJB
	private GroupDaoBean groupDaoBean;
	@Inject
	private SecurityContext securityContext;

	private User user;
	private User selected;
	private User personal;
	private List<User> users = null;
	private String firstName;
	private String lastName;
	private String email;
	private String username;
	private String password;
	private String oldpassword;
	private String newPassword;
	private Groups groups;
	private List<Groups> group = null;
	private Integer groupId = 2;
	private String passwordConfirm;
	private Groups groupsUser = new Groups(2, "user");
	private Groups groupsAdmin = new Groups(1, "admin");
	@Inject
	private PasswordHasher passwordHasher;

	public UserController() {
		user = new User();
		groups = new Groups();
	}

	public String saveUser() {
		String returnValue = "user_saved";
		try {
			populateUser();
			userDaoBean.saveUser(user);
		} catch (Exception e) {
			e.printStackTrace();
			returnValue = "error";
		}

		return returnValue;

	}

	public void deleteUserMessage() {

		String msg;
		msg = "Active User Cannot be Deleted!";
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", msg);
		FacesContext.getCurrentInstance().addMessage(msg, message);
	}

	public void addMessage(String summary, String detail) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	private void populateUser() {

		user = new Entities.User();
		user.setFirstName(getFirstName());
		user.setLastName(getLastName());
		user.setEmail(getEmail());
		user.setUsername(getUsername());
		user.setPassword(this.passwordHasher.hashPassword(getPassword()));
		user.setGroups(groupsUser);

	}

	public void setToUser(Groups groups) {
		this.groupsUser = groups;
	}

	public void setToAdmin(Groups groups) {
		this.groupsAdmin = groups;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public User getSelected() {
		return selected;
	}

	public void setSelected(User selected) {
		this.selected = selected;
		System.out.println(this.selected.getFirstName());
	}

	public List<User> getUsers() {
		users = userDaoBean.findAll();
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public Groups getGroups() {
		return groups;
	}

	public void setGroups(Groups groups) {
		this.groups = groups;
	}

	public User getPersonal() {

		return personal;
	}

	public void setPersonal(User personal) {
		System.out.println("Personal: " + this.personal.getFirstName());
		this.personal = personal;
	}

	public User getUser() {
		personal = userDaoBean.findUserByUsername(securityContext.getCallerPrincipal().getName());
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getOldpassword() {
		return oldpassword;
	}

	public void setOldpassword(String oldpassword) {
		this.oldpassword = oldpassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String updateRole(User usr) {
		String returnValue = "update_toUser";
		String returnValue2 = "update_toAdmin";
		if (usr.getUsername().equals(securityContext.getCallerPrincipal().getName())) {
			changeRoleMessage();
			return returnValue = "user_list";
		}
		setSelected(usr);
		this.setSelected(getSelected());

		if (usr.getGroups().getGroupId() == 2) {
			return returnValue2;
		}
		return returnValue;
	}

	public String deleteUser(User us) {
		String returnValue = "user_list";
		if (us.getUsername().equals(securityContext.getCallerPrincipal().getName())) {
			deleteUserMessage();
			return returnValue;
		}
		try {
			userDaoBean.deleteUser(us);
		} catch (Exception e) {
			e.printStackTrace();
			returnValue = "error";
		}

		return returnValue;
	}

	public String updateUser(User us) {
		String returnValue = "update_user";
		setSelected(us);
		this.setSelected(getSelected());

		return returnValue;
	}

	public String editProfile() {
		String returnValue = "edit_profile";
		this.user.toString();
		return returnValue;
	}

	public String editPassword() {
		String returnValue = "edit_password";

		return returnValue;
	}

	public String saveUpdated() {
		String returnValue = "user_updated";
		try {
			userDaoBean.updateUser(this.selected);
		} catch (Exception e) {
			e.printStackTrace();
			returnValue = "error";
		}

		return returnValue;
	}

	public String saveUpdatedPersonal() {
		String returnValue = "user_personalUpdated";
		try {
			userDaoBean.updateUser(this.personal);
		} catch (Exception e) {
			e.printStackTrace();
			returnValue = "error";
		}

		return returnValue;
	}

	public String saveUpdatedRoleAdmin() {
		String returnValue = "user_updated";
		try {
			selected.setGroups(groupsAdmin);
			userDaoBean.updateUser(this.selected);
		} catch (Exception e) {
			e.printStackTrace();
			returnValue = "error";
		}

		return returnValue;
	}

	public String saveUpdatedRoleUser() {
		String returnValue = "user_updated";

		try {
			selected.setGroups(groupsUser);
			userDaoBean.updateUser(this.selected);
		} catch (Exception e) {
			e.printStackTrace();
			returnValue = "error";
		}

		return returnValue;
	}

	public void changeRoleMessage() {

		String msg;
		msg = "Active User Cannot be Changed!";
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", msg);
		FacesContext.getCurrentInstance().addMessage(msg, message);
	}

	public void success() {
		String msg;
		msg = "Password Updated!";
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Success!", msg);
		FacesContext.getCurrentInstance().addMessage(msg, message);
	}

	public void fail() {
		String msg;
		msg = "Update Password Failed!";
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", msg);
		FacesContext.getCurrentInstance().addMessage(msg, message);
	}

	public void warn() {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_WARN, "Check Current Password!", null));
	}

	public String saveUpdatedPassword() {
		String returnValue = "personal_data";
		String pass = this.getOldpassword();
		if (passwordCheck(pass) == false) {
			warn();
			return returnValue = "edit_password";
		}
		success();

		try {
			personal.setPassword(this.passwordHasher.hashPassword(getNewPassword()));
			userDaoBean.updateUser(personal);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnValue;
	}

	public List<Groups> getGroup() {
		groupDaoBean.findAll();
		return group;
	}

	public void setGroup(List<Groups> group) {
		this.group = group;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	public boolean passwordCheck(String password) {
		User usr = userDaoBean.findUserByUsername(personal.getUsername());
		String hashedPassword = usr.getPassword();
		char[] arr = password.toCharArray();
		return passwordHasher.checkPassword(arr, hashedPassword);

	}

}