package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "users")
@XmlRootElement
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "USER_ID", nullable = false)
	private Integer userId;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 10)
	@Column(name = "USERNAME")
	private String username;
	@Size(max = 15)
	@Column(name = "FIRST_NAME")
	private String firstName;
	@Size(max = 20)
	@Column(name = "LAST_NAME")
	private String lastName;
	@Size(max = 45)
	@Column(name = "EMAIL")
	private String email;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 160)
	@Column(name = "PASSWORD")
	private String password;
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="GROUPS")
	private Groups groups;
	
	
	public User() {
	}

	public User(String username) {
		this.username = username;
	}

	public User(Integer userId) {
		this.userId = userId;
	}

	public User(String username, String password) {

		this.username = username;
		this.password = password;
	}

	public User(Integer userId, String username, String password) {
		this.userId = userId;
		this.username = username;
		this.password = password;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (userId != null ? userId.hashCode() : 0);
		return hash;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", username=" + username + "password: " + password + " group= " + groups + "]";
	}
	@Enumerated(EnumType.STRING)
	public Groups getGroups() {
		return groups;
	}

	public void setGroups(Groups groups) {
		this.groups = groups;
	}
	
}
