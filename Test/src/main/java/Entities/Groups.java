package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "groups")
@XmlRootElement

public class Groups implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "GROUP_ID", nullable = false)
	private Integer groupId;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 10)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "GROUPNAME")
	private String groupName;
	
	
	public Groups() {
	}

	public Groups(Integer groupId) {
		this.groupId = groupId;
	}

	public Groups(Integer groupId, String groupName) {
		this.groupId = groupId;
		this.groupName = groupName;

	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	@Override
	public boolean equals(Object object) {

		if (!(object instanceof Groups)) {
			return false;
		}
		Groups other = (Groups) object;
		if ((this.groupId == null && other.groupId != null)
				|| (this.groupId != null && !this.groupId.equals(other.groupId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Group[ Group ID = " + groupId + " groupName=" + groupName + "]";
	}

}
